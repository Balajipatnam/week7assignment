create database ecommerce_balaji;
use ecommerce_balaji;
create table bookess(id int(20) primary key not null,title varchar(45),author varchar(45),year varchar(45));
desc bookess;
insert into bookess values(1,"Harry Potter","Rowling","1999");
insert into bookess values(2,"The Mother Toungue","Bill Bryson","2001");
insert into bookess values(3,"Lord Of The Rings","Jude Fisher","1992");
insert into bookess values(4,"Hatchet","Gary Paulsen","2011");
insert into bookess values(5,"Heidi","Frank","2008");
select *from bookess;

create table book_customer(email varchar(50) primary key not null,name varchar(50),phone varchar(20),password varchar(20)); 
desc book_customer;
insert into book_customer values("ramu@gmail.com","ramu","9876543210","ramu");
insert into book_customer values("charan@gmail.com","charan","9872143261","charan");
insert into book_customer values("chinna@gmail.com","chinna","9871143219","chinna");
insert into book_customer values("ravi@gmail.com","ravi","9876903278","ravi");
insert into book_customer values("rohit@gmail.com","rohit","9876546518","rohit");
select *from book_customer;

create table favtable(id int(20) primary key not null,title varchar(45),author varchar(45),year varchar(45));
desc favtable;
select *from favtable;
