package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

	public static Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/ecommerce","root","Mysql@123");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("error driver not loaded");
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("cannot connect to database");
			e.printStackTrace();
		}
		System.out.println("driver loaded and connected");

		return conn;	
	}
}
