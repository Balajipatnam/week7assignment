package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Book;

public class Book_Database {

	private static Connection con = DBConnection.getConnection();

	public List<Book> getAllItems() throws SQLException
	{
		List<Book> books = new ArrayList<>();
		String sql = "select  * from bookess";

		Statement statement = con.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		while(rs.next())
		{
			Book book = new Book();
			book.setId(rs.getInt(1));
			book.setTitle(rs.getString(2));
			book.setAuthor(rs.getString(3));
			book.setYear(rs.getString(4));
			books.add(book);
		}
		return books;
	}
	public List<Book> getAllFav() throws SQLException
	{
		List<Book> books = new ArrayList<>();
		String sql = "select  * from favtable";

		Statement statement = con.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		while(rs.next())
		{
			Book book = new Book();
			book.setId(rs.getInt(1));
			book.setTitle(rs.getString(2));
			book.setAuthor(rs.getString(3));
			book.setYear(rs.getString(4));
			books.add(book);
		}
		return books;
	}
	public Book getBookById(int id) throws SQLException
	{
		Book book = null;
		String sql = "select  * from Bookess where id='"+id+"'";
		//  statement
		Statement statement = con.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		if(rs.next())
		{
			book = new Book();
			book.setId(rs.getInt(1));
			book.setTitle(rs.getString(2));
			book.setAuthor(rs.getString(3));
			book.setYear(rs.getString(4));

		}
		System.out.println(book);
		return book;
	}
	public boolean favTable(String id) throws SQLException {

		String sql = "insert into favtable select * from bookess where id='"+id+"'";
		PreparedStatement statement = con.prepareStatement(sql);
		statement.executeUpdate();
		return true;

	}
}
