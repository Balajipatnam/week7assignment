package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.LoginUser;

public class LoginUserDatabase {

	public boolean validate(LoginUser user)
	{
		String sql = "select email, password from book_customer where email = ?";
		// establishing connection
		Connection conn = DBConnection.getConnection();
		PreparedStatement statement = null;
		try {
			statement = conn.prepareStatement(sql);
			statement.setString(1, user.getEmail());
			ResultSet rs = statement.executeQuery();
			String useremail = null,userpassword = null;
			if(rs.next()) {
				useremail = rs.getString(1);
				userpassword = rs.getString(2);
				if(user.getEmail().equals(useremail) && user.getPassword().equals(userpassword)) {
					return true;
				} 
				// throw invalid credential exception
			}
			// throw record does not exist exception

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return false;
	}
}
