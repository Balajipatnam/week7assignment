package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Book_Customer;

public class Book_CustomerDatabase {

	private static Connection con=DBConnection.getConnection();

	public List<Book_Customer> getAllCustomers() throws SQLException
	{
		// to display books
		List<Book_Customer> customers=new ArrayList<Book_Customer>();
		String sql="select email,name,phone,password from book_customer";
		Statement statement =con.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		while(rs.next())
		{

			Book_Customer customer = new Book_Customer();
			customer.setEmail(rs.getString(1));
			customer.setName(rs.getString(2));
			customer.setPhone(rs.getString(3));		
			customer.setPassword(rs.getString(4));
			customers.add(customer);
		}

		return customers;	
	}
	// to insert books
	public boolean insertCustomer(Book_Customer customer)throws SQLException{
		String prepstsql="insert into book_customer(email,name,phone,password)values(?,?,?,?)";
		PreparedStatement ps=con.prepareStatement(prepstsql);

		ps.setString(1,customer.getEmail());
		ps.setString(2,customer.getName());		
		ps.setString(3,customer.getPhone());	
		ps.setString(4,customer.getPassword());
		ps.executeUpdate();
		return true;
	}
}
