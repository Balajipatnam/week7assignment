package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.Book_CustomerDatabase;
import model.Book_Customer;


import static utility.Constants.*;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisterServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("register.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String email=request.getParameter(EMAIL);
		String name=request.getParameter(NAME);
		String phone=request.getParameter(PHONE);
		String password=request.getParameter(PASSWORD);

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		if( (!!email.isEmpty() || email != null) && 
				(name != null && !name.trim().isEmpty()) &&
				(!phone.isEmpty() && phone != null) &&
				(!password.isEmpty() && password != null))

		{
			System.out.println("if not null");
			Book_Customer c1 = new Book_Customer(email,name,phone,password);
			System.out.println(c1);
			Book_CustomerDatabase database=new Book_CustomerDatabase();
			try {
				if(database.insertCustomer(c1))
				{
					System.out.println("insert true");
					response.sendRedirect("login.jsp");
				}
				else {
					System.out.println("insert false");
					RequestDispatcher rd = request.getRequestDispatcher("register.jsp");

					out.println("Please contact administrator");
					rd.include(request, response);
				}
			} catch (SQLException | ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else{
			out.println("Fill in all details");
			RequestDispatcher rd = request.getRequestDispatcher("register.jsp");			
			rd.include(request, response);
			//response.sendRedirect("register.html");
		}
	}
}
