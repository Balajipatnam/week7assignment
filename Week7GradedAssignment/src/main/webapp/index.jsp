<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<style>
body {
	background-image:
		url(https://9to5mac.com/wp-content/uploads/sites/6/2019/07/apple_kaertnerstrasse_storeteller.jpg?quality=82&strip=all);
}

h1 {
	background-color: lightgrey;
	width: 400px;
	border: 5px solid slateblue;
	padding: 10px;
	color: Black;
	margin: auto;
	border-radius: 10px;
	text-align: center;
	font-family: 'Courier New', Courier, monospace;
}

.button {
	border: 2px solid black;
	background-color: black;
	color: white;
	padding: 15px 32px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	margin: 15px 2px;
	cursor: pointer;
	border-radius: 5px;
	font-weight: bold;
	margin-left: 25px;
	font-family: 'Courier New', Courier, monospace;
}

.container {
	display: flex;
	align-items: center;
	justify-content: center;
}

.button:hover {
	background-color: grey;
	color: white;
}
</style>
<body>
<h1>Online Book Store</h1>
	<div class='container'>
		<div>
			<a href='login.jsp' class="button">Login</a>
		</div>
		<div>
			<a href='register.jsp' class="button">Register</a>
		</div>
		<div>
			<a href='book1' class="button">List Of Books</a>
		</div>

	</div>
	<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
		crossorigin="anonymous"></script>
</body>
</html>