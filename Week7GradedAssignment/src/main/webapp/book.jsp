<%@page import="model.Book"%>
<%@page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%
	HttpSession sess = request.getSession();
	String email = (String) sess.getAttribute("email");
	if (email != null) {
	%>
	<h1>
		Welcome<%=email%></h1>
	<h3>
		<a href='books'>List Of Books</a>
	</h3>
	<h3>
		<a href='dashboard'>Dashboard</a>
	</h3>
	<h3>
		<a href='logout'>Logout</a>
	</h3>
	<h1>Available Books</h1>
	<%
	}
	%>
	<table border="1">
		<tr>
			<th>Id</th>
			<th>Title</th>
			<th>Author</th>
			<th>Year</th>
		</tr>
		<%
		List<Book> books = (List<Book>) request.getAttribute("list");
		for (Book book : books) {
		%>
		<tr>
			<td><p>
					Id:<%=book.getId()%></p></td>
			<td><p>
					Title:<%=book.getTitle()%></p></td>
			<td><p>
					Author:<%=book.getAuthor()%></p></td>
			<td><p>
					Year:<%=book.getYear()%></p></td>
			<td><a href="books?id=<%=book.getId()%>">Add to favr</a></td>

		</tr>
		<%
		}
		%>
	</table>
	<h3>
		<button type='button' onClick="location.href='favservlet'">View
			fav Books</button>

	</h3>
</body>
</html>