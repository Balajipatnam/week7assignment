<%@page import="model.Book"%>
<%@page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


	<h3>
		<a href='login'>Login</a>
	</h3>
	<h3>
		<a href='register'>Register</a>
	</h3>
	<h1>Available Books</h1>
	<table border="1">
		<tr>
			<th>Id</th>
			<th>Title</th>
			<th>Author</th>
			<th>Year</th>
		</tr>
		<%
		List<Book> books = (List<Book>) request.getAttribute("list");
		for (Book book : books) {
		%>
		<tr>
			<td><p>
					Id:<%=book.getId()%></p></td>
			<td><p>
					Title:<%=book.getTitle()%></p></td>
			<td><p>
					Author:<%=book.getAuthor()%></p></td>
			<td><p>
					Year:<%=book.getYear()%></p></td>
		</tr>
		<%
		}
		%>
	</table>
	
</body>
</html>